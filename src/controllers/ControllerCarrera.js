const { default: mongoose } = require("mongoose");
const Inventario = require("../models/inventario");


function prueba(req, res){
    res.status(200).send({
        message: 'haciendo una prueba'
    });
}

function saveInventario(req,res){
    var myInvetario=new Inventario(req.body);

    myInvetario.save((err,result)=>{
        res.status(200).send({message:result});
    });
    
}
function buscarData(req,res){
    var idInventario=req.params.id;
    Carrera.findById(idInvetario).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error al momento de ejecutar la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro a buscar no se encuentra disponible'});
            }else{
                res.status(200).send({result});
            }
        }
    });
}

function listarAllData(req,res){
    var idInventario=req.params.idb;
    if(!idInventario){
        var result=Inventario.find({}).sort('nombre');
    }else{
        var result=Inventario.find({carrera:idInventario}).sort('nombre');
    }

    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error al momento de ejecutar la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro a buscar no se encuentra disponible'});
            }else{
                res.status(200).send({result});
            }
        }
    });
}

function updateInventario(req,res){
    //var id = mongoose.Types.ObjectId(req.query.productId);
    var id = req.params.id;
    Carrera.findOneAndUpdate({_id: id}, req.body, {new: true},
        function(err, Inventario){
            if(err){
                res.send(err);
            }
            res.json(Carrera);
        });
}

function deleteInvetario(req,res){
    var idCarrera=req.params.id;
    Carrera.findByIdAndDelete(idCarrera).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error al momento de ejecutar la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro a buscar no se encuentra disponible'});
            }else{
                res.status(200).send({result});
            }
        }
    });
}


module.exports={
    prueba,
    saveInventario,
    listarAllData,
    buscarData,
    updateInventario,
    deleteInvetario
}